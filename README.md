README
------

Just a demo repository to test git + docker hub + deployment workflow.

### Clone git repository

	git clone git@bitbucket.org:cjungel/docker-nginx-test.git

### Make changes

	cd docker-nginx-test
	vim index.html # Change content

### Commit and push changes

	git commit -am "Changed conent."
	git push origin master

### Check docker hub

Image should be built.

### Tag your version

	git tag v.2.3.4
	git push origin --tags

### Check docker hub

Image should be built for new version.

### Run new image on a docker host

	docker run -d -p 80:80 --name docker-nginx-test cjungel/docker-nginx-test:v1.0.0



